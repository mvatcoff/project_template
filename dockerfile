FROM mcr.microsoft.com/devcontainers/cpp:ubuntu-20.04 AS host

LABEL maintainer="Mariano Vatcoff"
LABEL version="0.1"
LABEL description="This image is to built TEST"

# Install GCC 13 and G++ 13
RUN apt update
RUN apt install -y software-properties-common
RUN add-apt-repository -y ppa:ubuntu-toolchain-r/test
RUN apt update
RUN apt install -y gcc-13 g++-13
RUN update-alternatives --install \
      /usr/bin/gcc gcc /usr/bin/gcc-13 13 \
      --slave /usr/bin/g++ g++ /usr/bin/g++-13

# Install Doxygen
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
    doxygen graphviz git
RUN rm -rf /var/lib/apt/lists/*

WORKDIR /app
VOLUME ["/app"]
