path_src=src
path_obj=$(path_src)/obj
path_lib=$(path_src)/lib
path_rls=release

f_main=main.cpp

release_name=test
version=beta
exe=$(release_name)_$(version)

cc=g++ -Wall -Iinclude

obj=$(path_obj)/cls_test.o


$(path_rls)/$(exe): $(path_src)/$(f_main) $(obj)
	$(cc) $(obj) $(path_src)/$(f_main) -o $(path_rls)/$(exe)

$(path_obj)/cls_test.o: $(path_lib)/cls_test.cpp
	$(cc) -c $(path_lib)/cls_test.cpp -o $(path_obj)/cls_test.o

clean:
	rm $(obj)

run:
	$(path_rls)/$(exe)