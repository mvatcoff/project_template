#ifndef _BATTERY_H_
#define _BATTERY_H_

#include <iostream>

/**
 * Example Class to understand C++ and the file hierarchy
 *
 */
class cls_test{
    public:
    cls_test();
    ~cls_test();

    void up();
};

#endif
